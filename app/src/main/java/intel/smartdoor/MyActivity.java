package intel.smartdoor;

import android.os.Bundle;
import android.os.RemoteException;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.estimote.sdk.Utils;

import java.util.List;


public class MyActivity extends ActionBarActivity {

    TextView textView_lock;
    ImageView imageview_lock;
    BeaconManager beaconManager;
    String beacon_door = "E1:B4:6D:6C:E3:0D";

    private static final Region ALL_ESTIMOTE_BEACONS = new Region("regionId", null, null, null);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        textView_lock = (TextView) findViewById(R.id.textview_lock);
        imageview_lock = (ImageView) findViewById(R.id.imageview_lock);

        beaconManager = new BeaconManager(getApplicationContext());
        beaconManager.setBackgroundScanPeriod(10, 0);

        textView_lock.setText("Door is locked");
        imageview_lock.setBackgroundResource(R.drawable.lock_close);

        beaconManager.setRangingListener(new BeaconManager.RangingListener() {
            @Override
            public void onBeaconsDiscovered(Region region, final List<Beacon> beacons) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (beacons.size() < 1) {
                            textView_lock.setText("Door is locked");
                            imageview_lock.setBackgroundResource(R.drawable.lock_close);
                        } else {
                            for(Beacon beacon : beacons){
                                if(beacon.getMacAddress().equals(beacon_door)){
                                    Log.v("DISTANCE: ",String.valueOf(calculateDistance(beacon)));
                                    if(calculateDistance(beacon)!=0.5){
                                        textView_lock.setText("Door is open");
                                        imageview_lock.setBackgroundResource(R.drawable.lock_open);
                                    }
                                    else{
                                        textView_lock.setText("Door is locked");
                                        imageview_lock.setBackgroundResource(R.drawable.lock_close);
                                    }

                                }

                            }
                        }
                    }
                });
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                try {
                    beaconManager.startRanging(ALL_ESTIMOTE_BEACONS);
                } catch (RemoteException e) {
                }
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            beaconManager.stopRanging(ALL_ESTIMOTE_BEACONS);
        } catch (RemoteException e) {
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        beaconManager.disconnect();
    }

    public double calculateDistance(Beacon beacon){
        return Math.min(Utils.computeAccuracy(beacon), 0.5);
    }

}
